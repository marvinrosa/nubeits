<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {

    return view('welcome');

});

/*
Route::get('/about', function () {

    return "Hi about page";

});

Route::get('/contact', function () {

    return "Hi i am contact";

});

Route::get('/post/{id}/{name}',function ($id,$name){

   return "This is a post number ".$id." ".$name;

});


Route::get('admin/post/example', array('as'=>'admin.home',function(){

    $url=route('admin.home');

    return "This url is ".$url;


}));

*/

//Route::get('/post/{id}','PostController@index');

//Route::resource('post','PostController');

Route::get('/contact','PostController@contact');

Route::get('post/{id}','PostController@show_post');